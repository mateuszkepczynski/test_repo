package main.java.Heros;

import java.time.Instant;
import java.util.Date;

/**
 * Created by Kepa on 06.08.2017.
 */
public abstract class Heros {
    String name;
    double HP;
    double attackPoints;
    double defPoints;
    Side side;
    Date spawnDate;
    boolean isBoss;

    public Heros(String name, Side side) {
        this.name = name;
        this.HP = (1000);
        this.attackPoints = (100);
        this.defPoints = (50);
        this.side = side;
        this.spawnDate = Date.from(Instant.ofEpochSecond(1));
        this.isBoss = (false);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHP() {
        return HP;
    }

    public void setHP(double HP) {
        this.HP = HP;
    }

    public double getAttackPoints() {
        return attackPoints;
    }

    public void setAttackPoints(double attackPoints) {
        this.attackPoints = attackPoints;
    }

    public double getDefPoints() {
        return defPoints;
    }

    public void setDefPoints(double defPoints) {
        this.defPoints = defPoints;
    }

    public Side getSide() {
        return side;
    }

    public void setSide(Side side) {
        this.side = side;
    }

    public Date getSpawnDate() {
        return spawnDate;
    }

    public void setSpawnDate(Date spawnDate) {
        this.spawnDate = spawnDate;
    }

    public boolean isBoss() {
        return isBoss;
    }

    public void setBoss(boolean boss) {
        isBoss = boss;
    }

    public abstract void doSpecialMove(Heros heros);

    public abstract double getAttacked(double attackPoints);

    public void attack(Heros enemy) {
        double attacValue = getAttacked(this.attackPoints) - enemy.getDefPoints();
        if (attacValue<0){
            attacValue=0;
        }
        enemy.setHP(enemy.getHP() - attacValue);
    }
}
