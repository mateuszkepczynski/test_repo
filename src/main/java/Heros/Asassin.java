package main.java.Heros;

/**
 * Created by Kepa on 06.08.2017.
 */
public class Asassin extends Heros {
    private double attackPoints;

    public Asassin(String name, Side side) {
        super(name, side);
        this.attackPoints = getAttackPoints()*2.5;
    }

    @Override
    public String toString() {
        return "Asassin{" +
                "attackPoints=" + attackPoints +
                ", name='" + name + '\'' +
                ", HP=" + HP +
                ", defPoints=" + defPoints +
                ", side=" + side +
                ", isBoss=" + isBoss +
                '}';
    }

    public void SpecialMove() throws InterruptedException {
        double currentHP = this.getHP();
        this.HP=this.getHP()+3000000;
        wait(10000L);
        this.HP = currentHP;
    }

    @Override
    public void doSpecialMove(Heros heros) {

    }

    @Override
    public double getAttacked(double attackPoints) {
        return this.attackPoints;
    }
}
