package main.java.Heros;

/**
 * Created by Kepa on 06.08.2017.
 */
public class Priest extends Heros {

    double HP;

    public Priest(String name, Side side) {
        super(name, side);
        this.HP = getHP()*1.5;
    }

    @Override
    public String toString() {
        return "Priest{" +
                "HP=" + HP +
                ", name='" + name + '\'' +
                ", attackPoints=" + attackPoints +
                ", defPoints=" + defPoints +
                ", side=" + side +
                ", isBoss=" + isBoss +
                '}';
    }

    public void SpecialMove() throws InterruptedException {
        this.attackPoints=this.attackPoints+2500;
        wait(10000L);
        this.attackPoints=this.attackPoints-2500;
    }

    @Override
    public void doSpecialMove(Heros heros) {

    }

    @Override
    public double getAttacked(double attackPoints) {
        return this.getAttackPoints();
    }

}
