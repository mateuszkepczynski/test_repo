package main.java.Heros;

/**
 * Created by Kepa on 06.08.2017.
 */
public class Tank extends Heros {
    private double HP;
    private double defPoints;

    public Tank(String name, Side side) {
        super(name, side);
        this.HP = 2 * getHP();
        this.defPoints = getDefPoints() * 1.2;
    }

    @Override
    public String toString() {
        return "Tank{" +
                "HP=" + HP +
                ", defPoints=" + defPoints +
                ", name='" + name + '\'' +
                ", attackPoints=" + attackPoints +
                ", side=" + side +
                ", isBoss=" + isBoss +
                '}';
    }

    public void SpecialMove() throws InterruptedException {
        this.defPoints=this.getDefPoints()+3000;
        wait(10000L);
        this.defPoints=this.getDefPoints()-3000;
    }

    @Override
    public void doSpecialMove(Heros heros) {

    }

    @Override
    public double getAttacked(double attackPoints) {
        return this.getAttackPoints();
    }
}
