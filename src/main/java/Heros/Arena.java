package main.java.Heros;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Kepa on 06.08.2017.
 */
public class Arena {
    private List<Heros> goodGuys = new ArrayList<>();
    private List<Heros> badGuys = new ArrayList<>();

    public Arena() {
    }

    public void addHero(Heros hero) {
        if (hero.getSide().equals(Side.EVIL)) {
            badGuys.add(hero);
        } else {
            goodGuys.add(hero);
        }
    }

    public void becomeBoss() {
        int theOlderIndex = 0;
        Date spawnDateMax = Date.from(Instant.ofEpochSecond(0));
        for (int i = 0; i < badGuys.size(); i++) {
            if (spawnDateMax.compareTo(badGuys.get(i).getSpawnDate()) < 0) {
                spawnDateMax = badGuys.get(i).getSpawnDate();
                theOlderIndex = i;
            }
        }
        badGuys.get(theOlderIndex).setBoss(true);
        if (badGuys.get(theOlderIndex) instanceof Asassin) {
            badGuys.get(theOlderIndex).setAttackPoints(badGuys.get(theOlderIndex).getAttackPoints() * 2.5);
        }
        if (badGuys.get(theOlderIndex) instanceof Tank) {
            badGuys.get(theOlderIndex).setHP(badGuys.get(theOlderIndex).getHP() * 2.0);
            badGuys.get(theOlderIndex).setDefPoints(badGuys.get(theOlderIndex).getDefPoints() * 1.2);
        }
        if (badGuys.get(theOlderIndex) instanceof Priest) {
            badGuys.get(theOlderIndex).setHP(badGuys.get(theOlderIndex).getHP() * 1.5);
        }
    }

    public void printHeros() {
        System.out.println("Drużyna Good Guys: ");
        for (Heros guy : goodGuys) {
            System.out.println(guy.toString());
        }
        System.out.println("Drużyna Bad Guys: ");
        for (Heros guy : badGuys) {
            System.out.println(guy.toString());
        }

    }

    public void fight(ArrayList<Heros> goodGuys, ArrayList<Heros> badGuys) {
        Random random = new Random();
        goodGuys.get(random.nextInt(goodGuys.size()));
        badGuys.get(random.nextInt(badGuys.size()));
    }

    public static void main(String[] args) {
        Asassin asassin = new Asassin("assasin", Side.GOOD);
        System.out.println(asassin instanceof Asassin);

    }
}

