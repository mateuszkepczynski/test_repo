package main.java.Store;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-07.
 */
public class MainMagazine {
    public static void main(String[] args) {
        Magazine magazine = new Magazine();
        Scanner scanner = new Scanner( System.in );
        boolean quit = true;
        do {
            try {
                try {
                    try {
                        try {
                            System.out.println( "Podaj komendę" );
                            String[] commend = scanner.nextLine().split( " " );
                            switch (commend[0]) {
                                case ("insert"):
                                    magazine.addProduct( commend[1], Double.parseDouble( commend[2] ), ProductShelf.valueOf( commend[3].toUpperCase() ), ProductType.valueOf( commend[4].toUpperCase() ) );
                                    break;
                                case ("show_type"):

                                    List<Product> byType = magazine.getByType( ProductType.valueOf( commend[1].toUpperCase() ) );
                                    for (Product product : byType) {
                                        System.out.println( product );
                                    }
                                    break;
                                case ("show_shelf"):
                                    List<Product> byShelf = magazine.getByShelf( ProductShelf.valueOf( commend[3].toUpperCase() ) );
                                    for (Product product : byShelf) {
                                        System.out.println( product );
                                    }
                                    break;
                                case ("save"):
                                    magazine.saveToExcel();
                                    break;
                            }
                            if (commend[0].equals( "quit" )) {
                                quit = false;
                            }
                        }catch (FileNotFoundException fnfe){
                            System.out.println(fnfe);
                        }
                    } catch (IllegalArgumentException iae) {
                        System.out.println( "Wprowadź poprawnie typ produktu i półkę na której się znajduje" );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (NumberFormatException nfe) {
                    System.out.println( "Wprowadź poprawnie cenę" );
                }
            } catch (ArrayIndexOutOfBoundsException aibe) {
                System.out.println( "Wprowadź poprawnie komendę" );
            }
        } while (quit);
    }
}

