package main.java.Store;

/**
 * Created by RENT on 2017-08-07.
 */
public class Product {
    private String name;
    private double price;
    private ProductShelf productShelf;
    private ProductType type;

    public Product(String name, double price, ProductShelf productShelf, ProductType type) {
        this.name = name;
        this.price = price;
        this.productShelf = productShelf;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public ProductShelf getProductShelf() {
        return productShelf;
    }

    public ProductType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", półka=" + productShelf +
                ", type=" + type +
                '}';
    }
}
