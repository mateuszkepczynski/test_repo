package main.java.Store;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by RENT on 2017-08-07.
 */
public class Magazine {

    Map<ProductType, List<Product>> mapMagazine = new HashMap<>();

    public void addProduct(String name, Double value, ProductShelf productShelf, ProductType productType) {
        Product product = new Product( name, value, productShelf, productType );

        if (mapMagazine.containsKey( productType )) {
            List<Product> productList = mapMagazine.get( productType );
            productList.add( product );
            mapMagazine.put( productType, productList );
        } else {
            List<Product> newProductList = new LinkedList<>();
            newProductList.add( product );
            mapMagazine.put( productType, newProductList );
        }
    }

    public List<Product> getByType(ProductType productType) {
        return mapMagazine.get( productType );
    }

    public List<Product> getByShelf(ProductShelf productShelf) {
        List<Product> filteredProduct = new LinkedList<>();
        for (List<Product> productTypeList : mapMagazine.values()) {
            for (Product singleProduct : productTypeList) {
                if (singleProduct.getProductShelf() == productShelf) {
                    filteredProduct.add( singleProduct );
                }
            }
        }
        return filteredProduct;
    }

    public void saveToExcel() throws IOException {
        String fileName = "out.xls";
        Workbook workbook = null;

        if(fileName.endsWith("xlsx")){
            workbook = new XSSFWorkbook();
        }else if(fileName.endsWith("xls")){
            workbook = new HSSFWorkbook();
        }
        Sheet sheet = workbook.createSheet("Products");
        int rowNumber = 0;
        for (List<Product> productClassList : mapMagazine.values()) {
            for (Product singleProduct : productClassList) {
                Row r = sheet.createRow(rowNumber);
                Cell c1 = r.createCell(0);
                c1.setCellValue(singleProduct.getName());
                Cell c2 = r.createCell(1);
                c2.setCellValue(singleProduct.getPrice());
                Cell c3 = r.createCell(2);
                c3.setCellValue(singleProduct.getType().toString());
                Cell c4 = r.createCell(3);
                c4.setCellValue(singleProduct.getProductShelf().toString());

            }
        }
        FileOutputStream fos = new FileOutputStream(fileName);
        workbook.write(fos);
        fos.close();
    }

}
