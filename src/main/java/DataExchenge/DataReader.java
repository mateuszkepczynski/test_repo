package main.java.DataExchenge;

import main.java.SimpleFile.Form;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-05.
 */
public class DataReader {
    public static void main(String[] args) throws InterruptedException {
        File exchengeFile = new File( "plikwymiany.txt" );

        long lenghtMod = 0;
        boolean readFile = true;

        String readLine = "";
        String lastLine = "";

        do {
            Thread.sleep( 2000 );
            if (lenghtMod != exchengeFile.length()) {
                try (BufferedReader reader = new BufferedReader( new FileReader( exchengeFile ) )) {
                    do {
                        readLine = reader.readLine();
                        if (readLine != null) {
                             System.out.println( "Odebrano linię: " + readLine );

                            lastLine = readLine;
                            if (readLine.equals( "quit" )) {
                                readFile = false;
                            }
                        }
                    } while (readLine != null);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                lenghtMod = exchengeFile.length();
            }
        } while (readFile);

    }
}
