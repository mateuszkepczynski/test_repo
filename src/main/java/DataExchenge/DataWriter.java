package main.java.DataExchenge;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-05.
 */
public class DataWriter {

    public static void main(String[] args) {
        Scanner scanner = new Scanner( System.in );
        File exchengeFile = new File( "plikwymiany.txt" );
        String command = null;

        try (FileOutputStream fos = new FileOutputStream( exchengeFile, true )) {
            PrintWriter writer = new PrintWriter( fos );

            do {
                System.out.println( "Dodaj kolejną linię do pliku, lub wyjdź komendą 'quit'" );
                command = scanner.nextLine();
                writer.println( command );
                writer.flush();
            } while (!command.toUpperCase().equals( "QUIT" ));
            writer.close();
        } catch (IOException fnfe) {
            System.out.println( "Exception : " + fnfe.getMessage() );
        }
    }
}
