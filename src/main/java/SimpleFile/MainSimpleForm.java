package main.java.SimpleFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-01.
 */

public class MainSimpleForm {
    public static void main(String[] args) {
        System.out.println( "Zaczynamy." );
        File naszPlik = new File( "test.txt" );
        if (naszPlik.exists()) {
            System.out.println( "Plik istnieje." );
        } else {
            System.out.println( "Plik nie istnieje." );
        }
        Scanner scanner = new Scanner( System.in );

        String com = "";

        try (FileOutputStream fos = new FileOutputStream( naszPlik, true )) {
            PrintWriter writer = new PrintWriter( fos );

            do {
                Form form = new Form();

                String name;
                System.out.println( "Podaj swoje imię." );
                name = scanner.nextLine();
                form.setName( name );

                String surname;
                System.out.println( "Podaj swoje nazwisko." );
                surname = scanner.nextLine();
                form.setSurname( surname );

                String sex;
                System.out.println( "Podaj swoją płeć." );
                sex = scanner.nextLine();
                form.setSex( sex );

                String ageString;
                Integer age;
                age = null;
                do {
                    System.out.println( "Podaj swój wiek." );
                    ageString = scanner.nextLine();

                    try {
                        age = Integer.parseInt( ageString );
                    } catch (NumberFormatException e) {
                        System.out.println( "Błędny format danych" );
                    }

                } while (age == null);

                form.setAge( age );


                if (sex.toUpperCase().equals( "K" ) && age >= 18 && age <= 25 || sex.toUpperCase().equals( "M" ) && age >= 25 && age <= 30) {
                    String answerK1;
                    System.out.println( "Czy chodujesz kwiatki?" );
                    answerK1 = scanner.nextLine();
                    form.setAnswer1( answerK1 );
                    String answerK2;
                    System.out.println( "Czy jesteś wolna/y?" );
                    answerK2 = scanner.nextLine();
                    form.setAnswer2( answerK2 );
                    String answerK3;
                    System.out.println( "Czy masz zwierzątko domowe?" );
                    answerK3 = scanner.nextLine();
                    form.setAnswer3( answerK3 );
                }else{
                    System.out.println("Nie łapiesz się do ankiety :(");
                }
                writer.println(form);
                writer.println( "___________________________________________________________________________" );

                System.out.println( "Czy chcesz dodadć kolejnego użytkownika? (tak/nie)" );
                com = scanner.nextLine();
            } while (!com.toUpperCase().equals( "NIE" ));
            writer.close();
        } catch (IOException fnfe) {
            System.out.println( "Exception : " + fnfe.getMessage() );
        }
    }
}
