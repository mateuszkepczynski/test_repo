package main.java.SimpleFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.List;
import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RENT on 2017-08-01.
 */
public class MainPathName {
    public static void main(String[] args) throws IOException {
        System.out.println( "Zaczynamy." );
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( "yyyy-MM-dd kk:mm:ss" );
        System.out.println();
        Scanner scanner = new Scanner( System.in );

        System.out.println( "Wprowadź komendę" );
        String com = scanner.nextLine();
        String[] comParser = com.split( " " );
        if (comParser[0].equals( "delete" )) {

            File file = new File( comParser[1] );
            if(file.delete()){
                System.out.println("usunięto plik");
            }else {
                System.out.println("nie udało się usunąć pliku");
            }

        } else if (comParser[0].equals( "create_folder" )) {

            File file = new File( comParser[1] );

            if(file.mkdir()){
                System.out.println("utworzono folder");
            }else {
                System.out.println("nie utworzono folderu");
            }

        } else if (comParser[0].equals( "create_file" )) {

            File file = new File( comParser[1] );

            if(file.createNewFile()){
                System.out.println("utworzono plik");
            }else{
                System.out.println("nie utworzono pliku");
            }

        } else {
            File naszPlik = new File( comParser[0] );

            if (!naszPlik.isDirectory()) {
                if (naszPlik.exists()) {
                    System.out.println( "Plik istnieje." );
                    System.out.println( "Ścieżka relatywna: " + naszPlik.getPath() );
                    System.out.println( "Ścieżka absolutna: " + naszPlik.getAbsolutePath() );
                    System.out.println( "Wielkość pliku: " + naszPlik.length() );
                    System.out.println( "Data ostatnniej modyfikacji: " + naszPlik.lastModified() );
                    System.out.println( "Czy plik jest ukryty: " + naszPlik.isHidden() );
                    System.out.println( "Prawo dostępu do odczytu: " + naszPlik.canRead() );
                    System.out.println( "Prawo dostępu do zapisu: " + naszPlik.canWrite() );
                    System.out.println( "Prawo dostępu do wykonywania: " + naszPlik.canExecute() );
                } else {
                    System.out.println( "Plik nie istnieje." );
                }
            } else {
                for (File file : naszPlik.listFiles()) {
                    System.out.println( String.format( String.format( "Nazwa pliku: " + file.getName() + " Wielkość pliku: " + file.length() + " " + "Data ostatnniej modyfikacji: " + simpleDateFormat.format( (file.lastModified()) ) ) ) );
                }
            }
        }
    }
}


