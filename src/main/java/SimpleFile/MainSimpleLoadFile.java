package main.java.SimpleFile;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-02.
 */
public class MainSimpleLoadFile {

    public static void main(String[] args) {

        Map<String, Map> mapOfMap = new HashMap<>();
        File naszPlik = new File( "test.txt" );
        Scanner scanner = new Scanner( System.in );
        String name;
        String surname;

        try (BufferedReader reader = new BufferedReader( new FileReader( naszPlik ) )) {
            String liniaZPliku = "";
            Form form = new Form();
            do {
                liniaZPliku = reader.readLine();

                if (liniaZPliku != null) {

                    if (!liniaZPliku.equals( "___________________________________________________________________________" )) {

                        String[] fildSplit = liniaZPliku.split( "= " );
                        switch (fildSplit[0]) {
                            case ("name"):
                                form.setName( fildSplit[1] );
                                break;
                            case ("surname"):
                                form.setSurname( fildSplit[1] );
                                break;
                            case ("age"):
                                form.setAge( Integer.parseInt( fildSplit[1] ) );
                                break;
                            case ("sex"):
                                form.setSex( fildSplit[1] );
                                break;
                            case ("answer1"):
                                form.setAnswer1( fildSplit[1] );
                                break;
                            case ("answer2"):
                                form.setAnswer2( fildSplit[1] );
                                break;
                            case ("answer3"):
                                form.setAnswer3( fildSplit[1] );
                                break;
                        }
                    } else {
                        Map<String, Form> map = new HashMap<>();
                        map.put( form.getName(), form );
                        mapOfMap.put( form.getSurname(), map );
                        form = new Form();
                    }
                }

            } while (liniaZPliku != null);
        } catch (IOException ioe) {

        }

        System.out.println( "Wprowadzone nazwisko: " );
        surname = scanner.next();
        System.out.println( "Wprowadzone imię: " );
        name = scanner.next();
        System.out.println( mapOfMap.get( surname ).get( name ) );

    }


}
