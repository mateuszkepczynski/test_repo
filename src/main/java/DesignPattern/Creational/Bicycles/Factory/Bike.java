package main.java.DesignPattern.Creational.Bicycles.Factory;



/**
 * Created by RENT on 2017-08-08.
 */
public class Bike {
    protected String Brand;
    protected int sits;
    protected int gears;
    protected BikeType bikeType;

    public Bike(String brand, int sits, int gears, BikeType bikeType) {
        this.Brand = brand;
        this.sits = sits;
        this.gears = gears;
        this.bikeType = bikeType;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public int getSits() {
        return sits;
    }

    public void setSits(int sits) {
        this.sits = sits;
    }

    public int getGears() {
        return gears;
    }

    public void setGears(int gears) {
        this.gears = gears;
    }

    public BikeType getBikeType() {
        return bikeType;
    }

    public void setBikeType(BikeType bikeType) {
        this.bikeType = bikeType;
    }
}
