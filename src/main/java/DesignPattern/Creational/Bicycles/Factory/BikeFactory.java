package main.java.DesignPattern.Creational.Bicycles.Factory;

/**
 * Created by RENT on 2017-08-08.
 */
public abstract class BikeFactory {

    public static Bike create5GearKrossBike(){
        return new Bike("KROSS", 1, 5, BikeType.SINGLE);
    }

    public static Bike create6GearMeridaBike(){
        return new Bike("Merida", 1, 6,BikeType.SINGLE);
    }

    public static Bike create3GearInianaBike(){
        return new Bike("INIANA", 2, 3, BikeType.TANDEM);
    }

    public static Bike create6GearFeltBike(){
        return new Bike("Felt", 1, 6, BikeType.SINGLE);
    }

    public static Bike create1GeaGoetzeBike(){
        return new Bike("Goetze", 2, 1, BikeType.TANDEM);
    }
}
