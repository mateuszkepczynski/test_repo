package main.java.DesignPattern.Creational.Computers.classes;


/**
 * Created by RENT on 2017-08-08.
 */
public class AsusPC extends AbstractPC {

    public AsusPC(String computerName, ComputerBrand computerBrand, int cpupower, double gpupower, boolean isOverClocked) {
        super( computerName, computerBrand, cpupower, gpupower, isOverClocked );
    }

    public static AbstractPC createAsusMaxPC( ){
        return new AsusPC("AsusMaxPC", ComputerBrand.ASUS, 100,100.0,false );
    }
    public static AbstractPC createAsusMinPC( ){
        return new AsusPC("AsusMinPC", ComputerBrand.ASUS, 30,30.0,false );
    }

    @Override
    public String toString() {
        return "AsusPC{" +
                "computerName='" + computerName + '\'' +
                ", computerBrand=" + computerBrand +
                ", cpupower=" + cpupower +
                ", gpupower=" + gpupower +
                ", isOverClocked=" + isOverClocked +
                '}';
    }
}
