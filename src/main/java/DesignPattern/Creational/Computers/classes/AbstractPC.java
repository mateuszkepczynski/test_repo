package main.java.DesignPattern.Creational.Computers.classes;

/**
 * Created by RENT on 2017-08-08.
 */
public abstract class AbstractPC {
    protected String computerName;
    protected ComputerBrand computerBrand;
    protected int cpupower;
    protected double gpupower;
    protected boolean isOverClocked;

    public AbstractPC(String computerName, ComputerBrand computerBrand, int cpupower, double gpupower, boolean isOverClocked) {
        this.computerName =computerName;
        this.computerBrand = computerBrand;
        if(cpupower<=100&&cpupower>=0) {
            this.cpupower = cpupower;
        }
        if(gpupower<=100.0&&gpupower>=0.0) {
            this.gpupower = gpupower;
        }
        this.isOverClocked = isOverClocked;
    }
}
