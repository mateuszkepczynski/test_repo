package main.java.DesignPattern.Creational.Computers.classes;

/**
 * Created by RENT on 2017-08-08.
 */
public class MacPC extends AbstractPC {
    public MacPC(String computerName, ComputerBrand computerBrand, int cpupower, double gpupower, boolean isOverClocked) {
        super( computerName, computerBrand, cpupower, gpupower, isOverClocked );
    }
    public AbstractPC createMacBookPro( ){
        return new AsusPC("MacBookPro", ComputerBrand.APPLE, 100,100.0,false );
    }
    public AbstractPC createAsusMinPC( ){
        return new AsusPC("MacBookAir", ComputerBrand.APPLE, 30,30.0,false );
    }
}
