package main.java.DesignPattern.Creational.Computers.Main;

import main.java.DesignPattern.Creational.Computers.classes.AbstractPC;
import main.java.DesignPattern.Creational.Computers.classes.AsusPC;

/**
 * Created by RENT on 2017-08-08.
 */
public class Main {
    public static void main(String[] args) {
        AbstractPC asusPC = AsusPC.createAsusMaxPC();
        System.out.println(asusPC);
    }
}
