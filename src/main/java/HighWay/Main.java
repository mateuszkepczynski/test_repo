package main.java.HighWay;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-05.
 */
public class Main {
    public static void main(String[] args) {
        HighWay highWay = new HighWay();
        Scanner scanner = new Scanner( System.in );
        String command = "";
        while (!command.toLowerCase().equals( "exit" )) {
            System.out.println( "Wprowadź komendę" );
            command = scanner.nextLine();
            String[] commandSplit = command.split( " " );
            try{
            switch (commandSplit[0]) {
                case ("entry"):
                       if( highWay.VehicleEntry( commandSplit[1],highWay.switchCarType( commandSplit[2] ) )){
                           System.out.println("Samochód wjehał na autostradę");
                       }else{
                           System.out.println("Samochód już jest na autostradzie");
                       }
                    break;
                case ("out"):
                    String registrationNumber = highWay.searchVehicle( commandSplit[1] ).getRegistrationNumber();
                    Optional<Double> price = highWay.VehicleLeave( commandSplit[1] );
                    if (price.isPresent()) {
                        System.out.println( "Samochód o numerze rejestracyjnym: " + registrationNumber + " opuścił autostradę." );
                        System.out.println( "Cena za przejazd: " + price.get() );
                    } else {
                        System.out.println( "Nie ma samochodu na autostradzie." );
                    }

                    break;
                case ("check"):
                    highWay.searchVehicle( commandSplit[1] );
                    break;

            }
        }catch (ArrayIndexOutOfBoundsException aiobe){
                System.out.println("Błędnie wprowadzona komenda");
            }
        }
    }
}
