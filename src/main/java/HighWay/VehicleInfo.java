package main.java.HighWay;

import java.time.LocalDateTime;

/**
 * Created by RENT on 2017-08-05.
 */
public class VehicleInfo {
    private LocalDateTime entryTime;
    private String registrationNumber;
    private CarType carType;

    public VehicleInfo(String registrationNumber, CarType carType, LocalDateTime entryTime) {
        this.registrationNumber = registrationNumber;
        this.carType = carType;
        this.entryTime = entryTime;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public CarType getCarType() {
        return carType;
    }

    public LocalDateTime getEntryTime() {
        return entryTime;
    }

    @Override
    public String toString() {
        return "registrationNumber= " + registrationNumber + "\n" +
                "carType= " + carType + "\n" +
                "entryTime= " + entryTime + "\n";
    }
}
