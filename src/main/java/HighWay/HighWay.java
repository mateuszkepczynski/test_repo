package main.java.HighWay;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by RENT on 2017-08-05.
 */
public class HighWay {
    private Map<String, VehicleInfo> vehicleInfoMap = new HashMap<>();

    public boolean VehicleEntry(String registrationNumber, CarType carType) {
        if (!vehicleInfoMap.containsKey( registrationNumber )) {
            vehicleInfoMap.put( registrationNumber, new VehicleInfo( registrationNumber, carType, LocalDateTime.now() ) );
            return true;
        } else {
            return false;
        }
    }

    public VehicleInfo searchVehicle(String registrationNumber) {

        return vehicleInfoMap.get( registrationNumber );

    }

    public Optional<Double> VehicleLeave(String registrationNumber) {
        if (!vehicleInfoMap.containsKey( registrationNumber )) {
            System.out.println( "Nie ma samochódu na autostradzie" );
            return Optional.empty();
        }
        LocalDateTime leaveTime = LocalDateTime.now();
        Timestamp tLeaveTime = Timestamp.valueOf( leaveTime );
        Timestamp tEntryTime = Timestamp.valueOf( vehicleInfoMap.get( registrationNumber ).getEntryTime() );
        long tDifferenceMin = (tLeaveTime.getTime() - tEntryTime.getTime()) / 1000;
        double payment = vehicleInfoMap.get( registrationNumber ).getCarType().getPriceMultiplayer()*tDifferenceMin;
        vehicleInfoMap.remove( registrationNumber );
        return Optional.of( new Double( payment ) );
    }
    public CarType switchCarType(String commandSplit) {
        switch (commandSplit) {
            case ("CAR"):
            case ("TRUCK"):
            case ("MOTORCYCLE"):
                return CarType.valueOf( commandSplit );
            default:
                return CarType.CAR;
        }
    }

}
