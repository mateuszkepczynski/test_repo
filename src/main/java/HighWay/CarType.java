package main.java.HighWay;

/**
 * Created by RENT on 2017-08-05.
 */
public enum CarType {
    TRUCK( 2.0 ), CAR( 1.0 ), MOTORCYCLE( 0.75 );
    private double priceMultiplayer;

    CarType(double priceMultiplayer) {
        this.priceMultiplayer = priceMultiplayer;
    }


    public double getPriceMultiplayer() {
        return priceMultiplayer;
    }

}
