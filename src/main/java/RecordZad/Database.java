package main.java.RecordZad;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by RENT on 2017-08-07.
 */
public class Database {
    int counter = 1;
    Map<Integer, Record> recordMap = new HashMap<>();

    public void addRecord(String name, LocalDateTime entryDate, LocalDateTime expirationDate) {
        recordMap.put( counter, new Record( counter, name, entryDate, expirationDate ) );
        System.out.println( "Dodano Record o ID: " + counter );
        counter++;
    }

    public Record serchRecord(int ID) {
        return recordMap.get( ID );
    }

    public void refresh() {
        for (Record record : recordMap.values()) {
            if (record.getExpirationDate().isAfter( LocalDateTime.now() )) {
                recordMap.remove( record );
                System.out.println("Usunięto"+record.toString());
            }
        }

    }
}
