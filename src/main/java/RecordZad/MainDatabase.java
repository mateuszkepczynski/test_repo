package main.java.RecordZad;

import java.text.Format;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-07.
 */
public class MainDatabase {
    public static void main(String[] args) {
        Database database = new Database();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");

        Scanner scanner = new Scanner( System.in );
        boolean quit = true;
        do{
            System.out.println("wprowadź komendę");
            String[] commend = scanner.nextLine().split(" ");
            switch (commend[0]) {
                case ("insert"):
                    LocalDateTime entryDate = LocalDateTime.parse(commend[2],formatter );
                    LocalDateTime expirationDate = LocalDateTime.parse(commend[3],formatter );
                    database.addRecord( commend[1],entryDate, expirationDate);
                    break;
                case ("serch"):
                    System.out.println(database.serchRecord( Integer.parseInt( commend[1] ) ));
                    break;
                case ("refresh"):
                  database.refresh();
                    break;
            }
            if (commend[0].equals( "quit" )) {
                quit = false;
            }
        }while (quit);


    }
}
