package main.java.RecordZad;


import java.time.LocalDateTime;

/**
 * Created by RENT on 2017-08-07.
 */
public class Record {
    int ID;
    String name;
    LocalDateTime entryDate;
    LocalDateTime expirationDate;

    public Record(int counter, String name, LocalDateTime entryDate, LocalDateTime expirationDate) {
        this.name = name;
        this.entryDate = entryDate;
        this.expirationDate = expirationDate;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getEntryDate() {
        return entryDate;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    @Override
    public String toString() {
        return "Record{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", entryDate=" + entryDate +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
