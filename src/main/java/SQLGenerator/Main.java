package main.java.SQLGenerator;

import main.java.Store.Product;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

/**
 * Created by RENT on 2017-08-26.
 */
public class Main {
    public static void main(String[] args) {
        Random random = new Random( );
        FileReader fileReader = new FileReader();
        RandomGnerator gnerator = new RandomGnerator();
        SQLFileWriter sqlFileWriter = new SQLFileWriter();
        fileReader.nameReader();
        fileReader.surnameReader();
        fileReader.produktReader();
        File exchengeFile = new File( "products.sql" );
        try (FileOutputStream fos = new FileOutputStream( exchengeFile)) {
            PrintWriter writer = new PrintWriter( fos );
            writer.print("");
            writer.close();
        } catch (IOException fnfe) {
            System.out.println( "Exception : " + fnfe.getMessage() );
        }
        for (int i = 0; i < 100; i++) {
            Person person = gnerator.randomPersonGenerator(fileReader);
            sqlFileWriter.filePersonWriter(person);
        }
        for (int i = 0; i < 100; i++) {
            Products product = gnerator.randomProductGenerator(fileReader);
            sqlFileWriter.fileProductWriter(product);
        }
    }
}
