package main.java.SQLGenerator;

import main.java.Store.Product;

import java.util.Random;

/**
 * Created by RENT on 2017-08-26.
 */
public class RandomGnerator {
    private static Random random = new Random();

    public static Person randomPersonGenerator(FileReader fileReader){
        int randomAge = random.nextInt(100);
        int arrayNameSize = fileReader.getNameList().size();
        String randomName = fileReader.getNameList().get(random.nextInt( arrayNameSize));
        int arraySurnameSize = fileReader.getSurnameList().size();
        String random_surname = fileReader.getSurnameList().get(random.nextInt(arraySurnameSize));
        Person person = new Person(randomName,random_surname,randomAge);
        return person;
    }

    public static Products randomProductGenerator(FileReader fileReader){
        int categoryID = random.nextInt(4);
        int arrayProductSize = fileReader.getProduktList().size();
        String randomName = fileReader.getProduktList().get(random.nextInt( arrayProductSize));
        Double price = random.nextDouble();
        Products products = new Products(randomName,price,categoryID);
        return products;
    }
}

