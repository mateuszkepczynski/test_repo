package main.java.SQLGenerator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by RENT on 2017-08-26.
 */
public class FileReader {
    private List<String> nameList = new ArrayList<>();
    private List<String> surnameList = new ArrayList<>();
    private List<String> produktList = new ArrayList<>();

    public List<String> getNameList() {
        return nameList;
    }

    public List<String> getSurnameList() {
        return surnameList;
    }

    public List<String> getProduktList() {
        return produktList;
    }

    public void load() {
        try (
                BufferedReader reader = new BufferedReader(new java.io.FileReader("Imiona.txt"))) {
                nameList = reader.lines().filter(s -> !s.isEmpty()).collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void nameReader() {
        String readLine;
        try (
                BufferedReader reader = new BufferedReader(new java.io.FileReader("Imiona.txt"))) {
            do {
                readLine = reader.readLine();
                if (readLine != null) {
                    nameList.add(readLine);
                }
            } while (readLine != null);
        } catch (
                FileNotFoundException e)

        {
            e.printStackTrace();
        } catch (
                IOException e)

        {
            e.printStackTrace();
        }
    }

    public void surnameReader() {
        String readLine;
        try (
                BufferedReader reader = new BufferedReader(new java.io.FileReader("Nazwiska.txt"))) {
            do {
                readLine = reader.readLine();
                if (readLine != null) {
                    surnameList.add(readLine);
                }
            } while (readLine != null);
        } catch (
                FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (
                IOException e)

        {
            e.printStackTrace();
        }
    }

    public void produktReader() {
        String readLine;
        try (
                BufferedReader reader = new BufferedReader(new java.io.FileReader("Produkty.txt"))) {
            do {
                readLine = reader.readLine();
                if (readLine != null) {
                    produktList.add(readLine);
                }
            } while (readLine != null);
        } catch (
                FileNotFoundException e)

        {
            e.printStackTrace();
        } catch (
                IOException e)

        {
            e.printStackTrace();
        }
    }



}
