package main.java.SQLGenerator;

/**
 * Created by RENT on 2017-08-26.
 */
public class Products {
    private String name;
    private double price;
    private int categoryid;

    public Products(String name, double price, int categoryid) {
        this.name = name;
        this.price = price;
        this.categoryid = categoryid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }
}
