package main.java.SQLGenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-26.
 */
public class SQLFileWriter {

    public void filePersonWriter(Person person) {
        File exchengeFile = new File( "person.sql" );
        try (FileOutputStream fos = new FileOutputStream( exchengeFile, true)) {
            PrintWriter writer = new PrintWriter( fos );
            writer.println( "Insert into person values ( "+"'"+person.getFirst_name()+"'"+","+"'"+person.getLast_name()+"'"+","+person.getAge()+");" );
            writer.close();
        } catch (IOException fnfe) {
            System.out.println( "Exception : " + fnfe.getMessage() );
        }
    }
    public void fileProductWriter(Products products) {
        File exchengeFile = new File( "products.sql" );
        try (FileOutputStream fos = new FileOutputStream( exchengeFile, true)) {
            PrintWriter writer = new PrintWriter( fos );
            writer.println( "Insert into products values ( "+"'"+products.getName()+"'"+","+products.getPrice()+","+products.getCategoryid()+");" );
            writer.close();
        } catch (IOException fnfe) {
            System.out.println( "Exception : " + fnfe.getMessage() );
        }
    }
}
